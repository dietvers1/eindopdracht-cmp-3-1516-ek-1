<?php
?>
<div id="main">
  <div id="header">
    <?php if ($page['header']): ?>
      <?php print render($page['header']); ?>
    <?php endif; ?>
    <div id="logo">
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
      <?php else: ?>
        <?php if ($site_name): ?>
          <h1 id="logo-text"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a></h1>
        <?php endif; ?>
      <?php endif; ?>
      <?php if ($site_slogan): ?>
        <p id="slogan"><?php print $site_slogan; ?></p>
      <?php endif; ?>
    </div> <!-- End Logo -->

    <div class="clear"></div>

    <?php if ($main_menu): ?>
    <div id="menu_first">
      <?php print theme('links__system_main_menu', array(
        'links' => $main_menu,
        'attributes' => array(
          'id' => 'main-menu-links',
          'class' => array('links', 'clearfix'),
        ),
        'heading' => array(
          'text' => t('Main menu'),
          'level' => 'h2',
          'class' => array('element-invisible'),
        ),
      )); ?>
    </div> <!-- End menu -->
    <?php endif; ?>

  </div> <!-- End header -->

  <div id="content-wrap">
    <?php if ($page['highlighted']): ?>
      <div id="highlighted"><?php print render($page['highlighted']); ?></div>
    <?php endif; ?>

    <div class="sidebar">
      <?php if ($page['sidebar_first']): ?>
        <?php print render($page['sidebar_first']); ?>
      <?php endif; ?>
    </div> <!-- End sidebar -->
    <div id="content">
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h2 class="node-title"><?php print $title; ?></h2>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php if ($tabs): ?>
        <div class="tabs"><?php print render($tabs); ?></div>
      <?php endif; ?>
      <?php if ($show_messages): ?>
        <?php print $messages; ?>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>

      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </div>
  </div> <!-- End content -->

  <div class="clear"></div>

  <div id="footer">
    <?php if ($page['footer']): ?>
      Copyright &copy; Dieter Verschaeren | <a href="http://dieter-v.byethost3.com/disclaimer/">Disclaimer | <a href="http://dieter-v.byethost3.com/privacy-policy/">Privacy Policy</a>
    <?php endif; ?>


  </div> <!-- End footer -->
  <div class="clear"></div>
</div>
